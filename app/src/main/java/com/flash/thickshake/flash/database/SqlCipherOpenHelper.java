package com.flash.thickshake.flash.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * A wrapper for a SQL lite databased; will be used to handle the encryption of the inside data.
 * Also will be used to properly detect and maintain updates to table structures.
 */
public class SqlCipherOpenHelper extends SQLiteOpenHelper {
    @SuppressWarnings("unused")
    private static final String TAG = SqlCipherOpenHelper.class.getSimpleName();

    private static final int MIGRATE_PREKEYS_VERSION = 1;
    private static final int MIGRATE_SESSIONS_VERSION = 1;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "flash.db";

    private final Context context;
    private final DatabaseSecret databaseSecret;

    public SqlCipherOpenHelper(@NonNull Context context, @NonNull DatabaseSecret databaseSecret) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, new SQLiteDatabaseHook() {
            @Override
            public void preKey(SQLiteDatabase db) {
                db.rawExecSQL("PRAGMA cipher_default_kdf_iter = 1;");
                db.rawExecSQL("PRAGMA cipher_default_page_size = 4096;");
            }

            @Override
            public void postKey(SQLiteDatabase db) {
                db.rawExecSQL("PRAGMA kdf_iter = '1';");
                db.rawExecSQL("PRAGMA cipher_page_size = 4096;");
            }
        });

        this.context = context.getApplicationContext();
        this.databaseSecret = databaseSecret;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO: Make a table entry for keys
        // db.execSQL(SmsDatabase.CREATE_TABLE);
        // executeStatements(db, SmsDatabase.CREATE_INDEXS);

        /* if (context.getDatabasePath(ClassicOpenHelper.NAME).exists()) {
            ClassicOpenHelper                      legacyHelper = new ClassicOpenHelper(context);
            android.database.sqlite.SQLiteDatabase legacyDb     = legacyHelper.getWritableDatabase();

            SqlCipherMigrationHelper.migratePlaintext(context, legacyDb, db);

            MasterSecret masterSecret = KeyCachingService.getMasterSecret(context);

            if (masterSecret != null) SQLCipherMigrationHelper.migrateCiphertext(context, masterSecret, legacyDb, db, null);
            else                      TextSecurePreferences.setNeedsSqlCipherMigration(context, true);

            if (!PreKeyMigrationHelper.migratePreKeys(context, db)) {
                ApplicationContext.getInstance(context).getJobManager().add(new RefreshPreKeysJob(context));
            }

            SessionStoreMigrationHelper.migrateSessions(context, db);
            PreKeyMigrationHelper.cleanUpPreKeys(context);
        }*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database: " + oldVersion + ", " + newVersion);

        db.beginTransaction();

        try {

            if (oldVersion < MIGRATE_PREKEYS_VERSION) {
                db.execSQL("CREATE TABLE signed_prekeys (_id INTEGER PRIMARY KEY, key_id INTEGER UNIQUE, public_key TEXT NOT NULL, private_key TEXT NOT NULL, signature TEXT NOT NULL, timestamp INTEGER DEFAULT 0)");
                db.execSQL("CREATE TABLE one_time_prekeys (_id INTEGER PRIMARY KEY, key_id INTEGER UNIQUE, public_key TEXT NOT NULL, private_key TEXT NOT NULL)");

                /*if (!PreKeyMigrationHelper.migratePreKeys(context, db)) {
                    ApplicationContext.getInstance(context).getJobManager().add(new RefreshPreKeysJob(context));
                }*/
            }

            if (oldVersion < MIGRATE_SESSIONS_VERSION) {
                db.execSQL("CREATE TABLE sessions (_id INTEGER PRIMARY KEY, address TEXT NOT NULL, device INTEGER NOT NULL, record BLOB NOT NULL, UNIQUE(address, device) ON CONFLICT REPLACE)");
                // SessionStoreMigrationHelper.migrateSessions(context, db);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if (oldVersion < MIGRATE_PREKEYS_VERSION) {
            // PreKeyMigrationHelper.cleanUpPreKeys(context);
        }
    }

    public SQLiteDatabase getReadableDatabase() {
        return getReadableDatabase(databaseSecret.asString());
    }

    public SQLiteDatabase getWritableDatabase() {
        return getWritableDatabase(databaseSecret.asString());
    }

    private void executeStatements(SQLiteDatabase db, String[] statements) {
        for (String statement : statements)
            db.execSQL(statement);
    }


}
