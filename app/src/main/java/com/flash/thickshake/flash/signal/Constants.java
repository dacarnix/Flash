package com.flash.thickshake.flash.signal;

/**
 * Holds the constants used for signal services
 */
public class Constants {
    public static final String URL = "https://my.signal.server.com";
}
