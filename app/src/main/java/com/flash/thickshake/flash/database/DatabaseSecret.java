/**
 * Copyright (C) 2011 Whisper Systems
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flash.thickshake.flash.database;

import android.support.annotation.NonNull;

import org.whispersystems.signalservice.internal.util.Hex;

import java.io.IOException;

public class DatabaseSecret {
    private final byte[] key;
    private final String encoded;

    public DatabaseSecret(@NonNull byte[] key) {
        this.key = key;
        this.encoded = Hex.toStringCondensed(key);
    }

    public DatabaseSecret(@NonNull String encoded) throws IOException {
        this.key = Hex.fromStringCondensed(encoded);
        this.encoded = encoded;
    }

    public String asString() {
        return encoded;
    }

    public byte[] asBytes() {
        return key;
    }
}
