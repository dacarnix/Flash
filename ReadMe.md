Flash is a new, fully encrypted and secure way to send temporary photos from your camera to your friends.

In essence, it is a fully secured version of "Snapchat", but of course, different enough in it's own right.
It will utilize end-to-end encryption, based on Signal, an open source encryption framework for mobile.